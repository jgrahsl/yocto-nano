# Yocto for Jetson Nano

`source sources/poky/oe-init-build-env build`
`bitbake core-image-full-cmdline`
`apt-get install python3-distutils gawk wget git-core diffstat unzip texinfo gcc-multilib build-essential chrpath socat`
`dpkg-reconfigure locales` `en_US.UTF-8`

Use branch for meta-rust
kazuki/fix/compat_kirkstone

```
git config user.name "Julian Grahsl"
git config user.email "julian@grahsl.net"
modprobe spidev
cp /usr/sbin/iptables-legacy /usr/sbin/iptables
```
